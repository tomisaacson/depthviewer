// This file is part of the Orbbec Astra SDK [https://orbbec3d.com]
// Copyright (c) 2015-2017 Orbbec 3D
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Be excellent to each other.
#include <SFML/Graphics.hpp>
#include <astra/astra.hpp>
#include "LitDepthVisualizer.hpp"
#include <chrono>
#include <iostream>
#include <iomanip>
#include <key_handler.h>
#include <sstream>


struct BinData
{
    int minX;
    int maxX;
    int minY;
    int maxY;
    int bottom;
    int top;
};

// col, row
    // // top row
    // update_percentage(1, 0, 192, 301, 63, 122, 760, 960);
    // update_percentage(2, 0, 325, 436, 67, 119, 760, 960);
    // update_percentage(3, 0, 460, 573, 67, 124, 760, 960);
static const BinData s_binData[4][4] = {
    {{0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}},
    {{192, 301, 63, 122, 760, 960}, {197, 295, 140, 195, 760, 960}, {192, 292, 213, 266, 760, 960}, {189, 291, 292, 351, 760, 960}},
    {{325, 436, 67, 119, 760, 960}, {320, 431, 142, 200, 760, 960}, {315, 429, 217, 270, 760, 960}, {313, 434, 289, 361, 760, 960}},
    {{460, 573, 67, 124, 760, 900}, {456, 574, 141, 201, 760, 960}, {456, 570, 218, 280, 760, 960}, {456, 576, 300, 367, 760, 960}}
};


class DepthFrameListener : public astra::FrameListener
{
public:
    DepthFrameListener()
    {
        prev_ = ClockType::now();
        font_.loadFromFile("Inconsolata.otf");
    }
    
    void init_texture(int width, int height)
    {
        if (!displayBuffer_ ||
            width != displayWidth_ ||
            height != displayHeight_)
        {
            displayWidth_ = width;
            displayHeight_ = height;
            // texture is RGBA
            const int byteLength = displayWidth_ * displayHeight_ * 4;
            displayBuffer_ = BufferPtr(new uint8_t[byteLength]);
            std::fill(&displayBuffer_[0], &displayBuffer_[0] + byteLength, 0);
            texture_.create(displayWidth_, displayHeight_);
            sprite_.setTexture(texture_, true);
            sprite_.setPosition(0, 0);
        }
    }

    void check_fps()
    {
        const float frameWeight = .2f;
        const ClockType::time_point now = ClockType::now();
        const float elapsedMillis = std::chrono::duration_cast<DurationType>(now - prev_).count();
        elapsedMillis_ = elapsedMillis * frameWeight + elapsedMillis_ * (1.f - frameWeight);
        prev_ = now;
        const float fps = 1000.f / elapsedMillis;
        const auto precision = std::cout.precision();
        // std::cout << std::fixed
        //           << std::setprecision(1)
        //           << fps << " fps ("
        //           << std::setprecision(1)
        //           << elapsedMillis_ << " ms)"
        //           << std::setprecision(precision)
        //           << std::endl;
    }

    void on_frame_ready(astra::StreamReader& reader,
                        astra::Frame& frame) override
    {
        const astra::PointFrame pointFrame = frame.get<astra::PointFrame>();
        const int width = pointFrame.width();
        const int height = pointFrame.height();
        init_texture(width, height);
        check_fps();
        if (isPaused_) { return; }
        copy_depth_data(frame);
        update_percentages();
        visualizer_.update(pointFrame);
        const astra::RgbPixel* vizBuffer = visualizer_.get_output();
        for (int i = 0; i < width * height; i++)
        {
            const int rgbaOffset = i * 4;
            displayBuffer_[rgbaOffset] = vizBuffer[i].r;
            displayBuffer_[rgbaOffset + 1] = vizBuffer[i].b;
            displayBuffer_[rgbaOffset + 2] = vizBuffer[i].g;
            displayBuffer_[rgbaOffset + 3] = 255;
        }
        texture_.update(displayBuffer_.get());
    }

    void copy_depth_data(astra::Frame& frame)
    {
        const astra::DepthFrame depthFrame = frame.get<astra::DepthFrame>();
        if (depthFrame.is_valid())
        {
            const int width = depthFrame.width();
            const int height = depthFrame.height();
            if (!depthData_ || width != depthWidth_ || height != depthHeight_)
            {
                depthWidth_ = width;
                depthHeight_ = height;
                // texture is RGBA
                const int byteLength = depthWidth_ * depthHeight_ * sizeof(uint16_t);
                depthData_ = DepthPtr(new int16_t[byteLength]);
            }
            depthFrame.copy_to(&depthData_[0]);
            // for (int x = 0; x < depthWidth_; x++)
            // {}
        }
    }

    void update_percentage(const int col, const int row, const int minX, const int maxX, const int minY, const int maxY, const int bottom, const int top)
    {
        short min = 10000;
        short max = 0;
        float total = 0;
        int valid = 0;
        int count = 0;
        for (int x = minX; x < maxX; x++)
        {
            for (int y = minY; y < maxY; y++)
            {
                count++;
                const size_t index = (depthWidth_ * y) + x;
                const short z = depthData_[index];
                // cout << z << ", ";
                if (z > 600) // distance from camera to freezer lid to cut out noise
                {
                    valid++;
                    total += z;
                    if (z < min) min = z;
                    if (z > max) max = z;
                }
            }            
        }
        float avg = total / valid;
        float floor = top - avg;
        float percent = 0;
        if (floor > 0) percent = (floor / (top - bottom)) * 100;
        percentages_[col][row] = percent;

        // std::string name = "r" + std::to_string(row) + "c" + std::to_string(col);
        // printf("%s: Pct: %2.2f, Avg: %2.2f, Min: %d, Max: %d, Valid: %d / %d\n", name.c_str(), percent, avg, min, max, valid, count);
    }

    void update_percentages()
    {
        for (int col = 0; col < 4; col++)
        {
            for (int row = 0; row < 4; row++)
            {
                if (s_binData[col][row].minX == 0)
                    continue;

                update_percentage(col, row,
                    s_binData[col][row].minX, s_binData[col][row].maxX,
                    s_binData[col][row].minY, s_binData[col][row].maxY,
                    s_binData[col][row].bottom, s_binData[col][row].top);
            }
        }
    }

    void update_mouse_position(sf::RenderWindow& window,
                               const astra::CoordinateMapper& coordinateMapper)
    {
        const sf::Vector2i position = sf::Mouse::getPosition(window);
        const sf::Vector2u windowSize = window.getSize();
        float mouseNormX = position.x / float(windowSize.x);
        float mouseNormY = position.y / float(windowSize.y);
        mouseX_ = depthWidth_ * mouseNormX;
        mouseY_ = depthHeight_ * mouseNormY;
        if (mouseX_ >= depthWidth_ ||
            mouseY_ >= depthHeight_ ||
            mouseX_ < 0 ||
            mouseY_ < 0) { return; }
        const size_t index = (depthWidth_ * mouseY_) + mouseX_;
        const short z = depthData_[index];
        coordinateMapper.convert_depth_to_world(float(mouseX_),
                                                float(mouseY_),
                                                float(z),
                                                mouseWorldX_,
                                                mouseWorldY_,
                                                mouseWorldZ_);
    }
    
    void draw_text(sf::RenderWindow& window,
                   sf::Text& text,
                   sf::Color color,
                   const int x,
                   const int y) const
    {
        // text.setColor(sf::Color::Black);
        // text.setPosition(x + 5, y + 5);
        // window.draw(text);
        text.setFillColor(color);
        text.setPosition(x, y);
        window.draw(text);
    }

    void draw_mouse_overlay(sf::RenderWindow& window,
                            const float depthWScale,
                            const float depthHScale) const
    {
        if (!isMouseOverlayEnabled_ || !depthData_) { return; }
        std::stringstream str;
        str << std::fixed
            << std::setprecision(0)
            << "(" << mouseX_ << ", " << mouseY_ << ") "
            << "X: " << mouseWorldX_ << " Y: " << mouseWorldY_ << " Z: " << mouseWorldZ_;
        const int characterSize = 40;
        sf::Text text(str.str(), font_);
        text.setCharacterSize(characterSize);
        text.setStyle(sf::Text::Bold);
        const float displayX = 10.f;
        const float margin = 10.f;
        const float displayY = window.getView().getSize().y - (margin + characterSize);
        draw_text(window, text, sf::Color::White, displayX, displayY);
    }

    void draw_percentages(sf::RenderWindow& window)
    {
        const sf::Vector2u windowSize = window.getSize();

        for (int col = 0; col < 4; col++)
        {
            for (int row = 0; row < 4; row++)
            {
                if (s_binData[col][row].minX == 0)
                    continue;

                float percent = percentages_[col][row];
                std::stringstream str;
                str << std::fixed
                    << std::setprecision(0)
                    << percent << "%";
                const int characterSize = 30;
                sf::Text text(str.str(), font_);
                text.setCharacterSize(characterSize);
                text.setStyle(sf::Text::Bold);

                float worldX = windowSize.x / depthWidth_ * s_binData[col][row].minX;
                float worldY = windowSize.y / depthHeight_ * s_binData[col][row].minY;
                draw_text(window, text, sf::Color::Red, worldX, worldY);
            }
        }
    }

    void draw_to(sf::RenderWindow& window)
    {
        if (displayBuffer_ != nullptr)
        {
            const float depthWScale = window.getView().getSize().x / displayWidth_;
            const float depthHScale = window.getView().getSize().y / displayHeight_;
            sprite_.setScale(depthWScale, depthHScale);
            window.draw(sprite_);
            draw_mouse_overlay(window, depthWScale, depthHScale);

            draw_percentages(window);
        }
    }

    void toggle_paused()
    {
        isPaused_ = !isPaused_;
    }

    bool is_paused() const
    {
        return isPaused_;
    }

    void toggle_overlay()
    {
        isMouseOverlayEnabled_ = !isMouseOverlayEnabled_;
    }

    bool overlay_enabled() const
    {
        return isMouseOverlayEnabled_;
    }

    void savePCD(const std::string& filename)
    {
	const int size = depthWidth_ * depthHeight_;

    FILE* fp = fopen(filename.c_str(), "wt");
    fprintf(fp, "VERSION .7\n");
    fprintf(fp, "FIELDS x y z\n");
    fprintf(fp, "SIZE 4 4 4\n");
    fprintf(fp, "TYPE F F F\n");
    fprintf(fp, "COUNT 1 1 1\n");
    fprintf(fp, "WIDTH %d\n", depthWidth_);
    fprintf(fp, "HEIGHT %d\n", depthHeight_);
    fprintf(fp, "VIEWPOINT 0 0 0 1 0 0 0\n");
    fprintf(fp, "POINTS %d\n", size);
    fprintf(fp, "DATA ascii\n");

    for (int y = 0; y < depthHeight_; y++)
    {
        for (int x = 0; x < depthWidth_; x++)
        {
            const size_t index = (depthWidth_ * y) + x;
            const short point = depthData_[index];
            if (point == 0)
            {
                fprintf(fp, "%d %d nan\n", x, y);
            }
            else
            {
               fprintf(fp, "%d %d %d\n", x, y, point);
            }
        }
    }
    fclose(fp);
    }

private:
    samples::common::LitDepthVisualizer visualizer_;
    using DurationType = std::chrono::milliseconds;
    using ClockType = std::chrono::high_resolution_clock;
    ClockType::time_point prev_;
    float elapsedMillis_{.0f};
    sf::Texture texture_;
    sf::Sprite sprite_;
    sf::Font font_;
    int displayWidth_{0};
    int displayHeight_{0};
    using BufferPtr = std::unique_ptr<uint8_t[]>;
    BufferPtr displayBuffer_{nullptr};
    int depthWidth_{0};
    int depthHeight_{0};
    using DepthPtr = std::unique_ptr<int16_t[]>;
    DepthPtr depthData_{nullptr};
    int mouseX_{0};
    int mouseY_{0};
    float mouseWorldX_{0};
    float mouseWorldY_{0};
    float mouseWorldZ_{0};
    bool isPaused_{false};
    bool isMouseOverlayEnabled_{true};

    float percentages_[4][4] {0};
};


astra::DepthStream configure_depth(astra::StreamReader& reader)
{
    auto depthStream = reader.stream<astra::DepthStream>();
    auto oldMode = depthStream.mode();
    //We don't have to set the mode to start the stream, but if you want to here is how:
    astra::ImageStreamMode depthMode;
    depthMode.set_width(640);
    depthMode.set_height(400);
    depthMode.set_pixel_format(astra_pixel_formats::ASTRA_PIXEL_FORMAT_DEPTH_MM);
    depthMode.set_fps(30);
    depthStream.set_mode(depthMode);
    depthStream.enable_mirroring(false);
    auto newMode = depthStream.mode();
    printf("Changed depth mode: %dx%d @ %d -> %dx%d @ %d\n",
        oldMode.width(), oldMode.height(), oldMode.fps(),
        newMode.width(), newMode.height(), newMode.fps());
    return depthStream;
}


int main(int argc, char** argv)
{
    astra::initialize();
    set_key_handler();
    sf::RenderWindow window(sf::VideoMode(1280, 800), "Depth Viewer");
#ifdef _WIN32
    auto fullscreenStyle = sf::Style::None;
#else
    auto fullscreenStyle = sf::Style::Fullscreen;
#endif
    const sf::VideoMode fullScreenMode = sf::VideoMode::getFullscreenModes()[0];
    const sf::VideoMode windowedMode(640, 400);
    bool isFullScreen = false;
    astra::StreamSet streamSet;
    astra::StreamReader reader = streamSet.create_reader();
    reader.stream<astra::PointStream>().start();
    auto depthStream = configure_depth(reader);
    depthStream.start();
    DepthFrameListener listener;
    reader.add_listener(listener);
    while (window.isOpen())
    {
        astra_update();
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::KeyPressed:
            {
                if (event.key.code == sf::Keyboard::C && event.key.control)
                {
                    window.close();
                }
                switch (event.key.code)
                {
                case sf::Keyboard::Escape:
                    window.close();
                    break;
                case sf::Keyboard::F:
                    if (isFullScreen)
                    {
                        window.create(windowedMode, "Depth Viewer", sf::Style::Default);
                    }
                    else
                    {
                        window.create(fullScreenMode, "Depth Viewer", fullscreenStyle);
                    }
                    isFullScreen = !isFullScreen;
                    break;
                case sf::Keyboard::R:
                    depthStream.enable_registration(!depthStream.registration_enabled());
                    break;
                case sf::Keyboard::M:
                    depthStream.enable_mirroring(!depthStream.mirroring_enabled());
                    break;
                case sf::Keyboard::P:
                    listener.toggle_paused();
                    break;
                case sf::Keyboard::S:
                    listener.savePCD("frame.pcd");
                    break;
                case sf::Keyboard::Space:
                    listener.toggle_overlay();
                    break;
                default:
                    break;
                }
                break;
            }
            case sf::Event::MouseMoved:
            {
                auto coordinateMapper = depthStream.coordinateMapper();
                listener.update_mouse_position(window, coordinateMapper);
                break;
            }
            default:
                break;
            }
        }
        // clear the window with black color
        window.clear(sf::Color::Black);
        listener.draw_to(window);
        window.display();
        if (!shouldContinue)
        {
            window.close();
        }
    }
    astra::terminate();
    return 0;
}
